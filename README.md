UCG
A Website For Get & Manage Information From Instagram

Pre-Installation
In Windows, MacOS or Linux we still need these packages to work with

Docker
Docker-compose


Installation
Clone The Project

git clone git@git.rabiloo.net:kigeki/instagram-insights.git
cd instagram-insights


Switch to Develop

git checkout develop


Copy .env file

cp .env.example .env


Setup Vendor Folder Via Docker

docker run --rm --interactive --tty \
  --volume $PWD:/app \
  composer install --ignore-platform-reqs


Run Sail In Background & Node Modules

vendor/bin/sail up -d
vendor/bin/sail npm install


Setup Key, Migration & Link Storage Folder

vendor/bin/sail artisan key:generate
vendor/bin/sail artisan migrate:fresh --seed
vendor/bin/sail artisan storage:link



Run
Run Sail consecutive (In case Sail not running in background)

vendor/bin/sail up


Render app.js, app.css for Vue

vendor/bin/sail npm run watch