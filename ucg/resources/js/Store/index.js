import { createStore } from 'vuex'
import { getField, updateField } from 'vuex-map-fields'
import _ from 'lodash';

export const store = createStore({
  state () {
    return {
      loading: false,
      name: '',
      user_id: '',
      type: ['instagram'],
      activeTab: 1,
      instaLinks: [
        {
          id: 1,
          link: 'https://www.instagram.com/p/CeUYEFtAKCr/',
          status: true,
          order: 2,
          beforeCrawl:false
        },
        {
          id: 5,
          link: 'https://www.instagram.com/p/CeUYEFtAKCr/213123',
          status: false,
          order: 1,
          beforeCrawl:true
        },

      ],
      twitLinks: [],
      reviews: [],
      instaCrawled: [],
      twitCrawled: [],
      pageTitle: ''
    }
  },
  getters: {
    getType(state) {
      return state.type
    },
    getName(state) {
      return state.name
    },

    getPageTitle: (state) => state.pageTitle,

    getField
  },

  mutations: {
    updateField,

    setPageTitle(state, pageTitle) {
      state.pageTitle = pageTitle
    },
    setType(state, value) {
      state.type = value
    },
    setName(state, value) {
      state.name = value
    },
    setUserId(state, value) {
      state.user_id = value
    },
    changeTab (state, n) {
      state.activeTab = n
    },
    addInstaLink(state) {
      console.log(state.instaLinks[state.instaLinks.length - 1].id + 1, 29);
      state.instaLinks.push({
        id: state.instaLinks[state.instaLinks.length - 1].id + 1,
        link: '',
        order: state.instaLinks.length,
        status: null,
        beforeCrawl: true,
      })
    },
    addTwitLink(state) {
      state.twitLinks.push({
        link: '',
        order: state.twitLinks.length,
        status: null,
        beforeCrawl: true
      })
    },
    addReviews(state) {
      state.reviews.push({
        username: '',
        date_publish: null,
        image_1: null,
        image_2: null,
        image_3: null,
        content: '',
        status: null,
        beforeCrawl: true
      })
    },
    importInstaLinks(state, links) {
      state.instaLinks.push(...links)
    },
    removeInstaLink(state, key) {
      state.instaLinks.splice(key, 1)
    },
    importTwitLinks(state, links) {
      state.twitLinks.push(...links)
    },
    removeTwitLink(state, key) {
      state.twitLinks.splice(key, 1)
    },
    removeReviews(state, key) {
      state.reviews.splice(key, 1)
    },
    changeLoading(state, value) {
      state.loading = value
    },
    changeImageReview(state, data) {
      switch (data.number) {
        case 1:
          state.reviews[data.index].image_1 = data.file
          break

        case 2:
          state.reviews[data.index].image_2 = data.file
          break

        case 3:
          state.reviews[data.index].image_3 = data.file
          break
      }
    },
    removeImageReview(state, data) {
      switch (data.number) {
        case 1:
          state.reviews[data.index].image_1 = ''
          break

        case 2:
          state.reviews[data.index].image_2 = ''
          break

        case 3:
          state.reviews[data.index].image_3 = ''
          break
      }
    },
    setDetailIframe(state, data) {
      state.activeTab = 1
      state.name = data.name
      state.user_id = data.user_id
      state.type = data.type
      state.instaLinks = data.instaLinks
      state.twitLinks = data.twitLinks
      state.reviews = data.reviews
    },
    setIframeDefault(state) {
      state.loading = false
      state.name = ''
      state.user_id = ''
      state.type = ['instagram']
      state.activeTab = 1
      state.instaLinks = []
      state.twitLinks = []
      state.reviews = []
      state.instaCrawled = []
      state.twitCrawled = []
    },
    setInstaCrawled(state, data) {
      state.instaCrawled = data;
      state.instaCrawled = data;
      const instalinks = _.clone(state.instaLinks);

      state.instaLinks = _.map(instalinks, (instalink) => {
        if (_.isEmpty(data)) {
          return {
            ...instalink,
            status: false,
            beforeCrawl: false
          }
        }

        const checkExists = _.findIndex(data, ['link', instalink.link]) > -1;

        return {
          ...instalink,
          status: checkExists,
          beforeCrawl: false
        }
      })
    },
    setTwitCrawled(state, data) {
      state.twitCrawled = data;
      const twitlinks = _.clone(state.twitLinks);

      state.twitLinks = _.map(twitlinks, (twitlink) => {
        if(_.isEmpty(data)){
          return {
            ...twitlink,
            status: false,
            beforeCrawl: false
          }
        }

        const checkExists = _.findIndex(data, ['link', twitlink.link]) > -1;

        return {
          ...twitlink,
          status: checkExists,
          beforeCrawl: false
        }
      })

    },
    updateOrderInstaLinks(state) {
      state.instaLinks = _.map(state.instaLinks, (instalink, index) => {
        return {
          ...instalink,
          order: index
        }
      })
    },
    updateOrderTwitLinks(state) {
      console.log(state.twitLinks)
      state.twitLinks = _.map(state.twitLinks, (twitLink, index) => {
        return {
          ...twitLink,
          order: index
        }
      })
    }
  },
  actions: {
    importInstagramCsv({ commit, state }, fileCsv) {
      commit('changeLoading', true)

      axios.post('/admin/import-instagram-link', fileCsv, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(response => {
        if (response.data.links.length) {
          let newLinks = []
          response.data.links.forEach(function (item) {
            newLinks.push({
              link: item,
              order: state.instaLinks.length,
              check: false
            })
          })

          commit('importInstaLinks', newLinks)
          commit('changeLoading', false)
        }
      })
    },
    importTwitterCsv({ commit, state }, fileCsv) {
      commit('changeLoading', true)

      axios.post('/admin/import-twitter-link', fileCsv, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(response => {
        if (response.data.links.length) {
          let newLinks = []
          response.data.links.forEach(function (item) {
            newLinks.push({
              link: item,
              order: state.twitLinks.length,
              check: false
            })
          })

          commit('importTwitLinks', newLinks)
          commit('changeLoading', false)
        }
      })
    },
    getIframeDetail({ commit }, iframe_id) {
      commit('changeLoading', true)

      axios.get('/admin/get-detail-iframe/' + iframe_id)
      .then(response => {
        if (response.data) {
          commit('setDetailIframe', response.data)
          commit('changeLoading', false)
        }
      })
    },
    getIframeDraft({ commit }, draft_id) {
      commit('changeLoading', true)

      axios.get('/admin/get-detail-draft/' + draft_id)
      .then(response => {
        if (response.data) {
          commit('setDetailIframe', response.data)
          commit('changeLoading', false)
        }
      })
    }
  }
})


