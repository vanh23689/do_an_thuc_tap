export default {

    validate: {
        required: 'Vui lòng không bỏ trống',
        instagram_empty: 'Instagram投稿のURLが入力されていません。',
        instagram_error: 'Instagram投稿のURLが無効です。',
        twitter_empty: 'Twitter投稿のURLが入力されていません。',
        twitter_error: 'Twitter投稿のURLが無効です。',
        review_empty: 'レビューが入力されていません。',
        review_error: 'レビューが無効です。',
        email_invalid: 'Email sai định dạng',
        email_exist: 'Email đã được đăng ký',
        password_invalid: 'Mật khẩu phải chứa ít nhất 6 ký tự và ít nhất 1 ký tự hoa và 1 số mỗi ký tự',
        password_same: 'Mật khẩu không giống nhau',
        url_company_invalid: 'URLの形式は正しくありません。',
        url_frame_invalid: 'URLの形式は正しくありません。',
    },
    alert: {
        success: {
            register_acccount: 'Tạo tài khoản thành công',
            invite_account: '招待メールに承認メールが送信されました。'
        },
        error: {
            invite_account: '招待メールに承認メールができませんでした。'
        }
    },
    table: {
        empty_text: '表示するデータがありません。',
        empty_matching: '該当結果が見つかりませんでした。'
    },
    announce: {
        success: '成功しました。',
        error: 'エラーが発生しました。'
    },

}
