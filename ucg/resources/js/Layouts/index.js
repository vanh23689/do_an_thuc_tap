import { createApp } from 'vue'
import Dashboard from '../Layouts/Dashboard.vue'
const app = createApp({})

app.
    component( 
  'dashboard-layout',
  Dashboard
)