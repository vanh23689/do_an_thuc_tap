<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            // Admin Seeder
            [
                'email' => 'admin@rabiloo.com',
                'email_verified_at' => Carbon::now(),
                'password' => bcrypt('a12345678X'),
                'username' => 'admin',
                'types' => 1,
                'department' => 'Administrator',
                'url_company' => 'example.com',
                'url_frame' => 'example.com',
                'responsible' => 'Admin',
                'company' => 'Rabiloo',
            ],
            // User Seeder
            [
                'email' => 'user@rabiloo.com',
                'email_verified_at' => Carbon::now(),
                'password' => bcrypt('a12345678X'),
                'username' => 'user',
                'types' => 2,
                'department' => 'Employee',
                'url_company' => 'example.com',
                'url_frame' => 'example.com',
                'responsible' => 'User',
                'company' => 'Rabiloo',
            ],
        ]);
    }
}
