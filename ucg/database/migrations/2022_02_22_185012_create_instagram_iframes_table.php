<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstagramIframesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_iframes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('link');
            $table->unsignedBigInteger('iframe_id');
            $table->tinyInteger('status')->nullable()->default('1');
            $table->foreign('iframe_id')->references('id')->on('iframes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_iframes');
    }
}
