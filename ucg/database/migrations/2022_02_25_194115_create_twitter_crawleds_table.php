<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTwitterCrawledsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twitter_crawleds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('link');
            $table->string('logo');
            $table->string('account')->nullable();
            $table->text('content')->nullable();
            $table->date('date')->nullable();
            $table->json('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twitter_crawleds');
    }
}
