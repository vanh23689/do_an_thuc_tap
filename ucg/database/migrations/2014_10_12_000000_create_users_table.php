<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->string('avatar')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('username')->unique(); // Only 1 Username
            $table->integer('types'); // 1. Admin // 2. Users (Default 2)
            $table->text('department')->nullable();
            $table->string('company')->nullable();
            $table->string('url_company')->nullable();
            $table->string('url_frame')->nullable();
            $table->string('responsible')->nullable(); // Nguời Đại Diện
            $table->softDeletes(); // Xoá Mềm
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
