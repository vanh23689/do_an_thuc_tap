<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\IframeController;
use App\Http\Controllers\Backend\AccountController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\Auth\LoginController;
use App\Http\Controllers\Backend\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Start Register
Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('register', [RegisterController::class, 'create'])->name('register.post');
// End Register
// Start Login
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'loginCheck'])->name('loginCheck');
// End Login
Route::post('logout', [LoginController::class, 'logout'])->name('logout');
    Route::get('/', [DashboardController::class, 'index'])->name('home');

    Route::get('/account-success', [AccountController::class, 'success'])->name('account-success');

    // User
    Route::resource('users', UserController::class);
    // Route::post('/delete-user', 'UserController@deleteUser')->name('delete-user');
    // Route::get('/api-detail-user/{user_id}', 'UserController@apiDetailUser')->name('api-detail-user');
    // Route::get('/api-get-users', 'UserController@apiGetUsers')->name('api-get-users');
    // Route::post('/export-users', 'UserController@exportUsers')->name('export-users');

    // // Iframe
    Route::resource('iframes', IframeController::class);
    // Route::resource('iframe-draft', 'IframeDraftController');
    // Route::post('crawl-instagram', 'CrawlController@crawlInstagram')->name('crawl.instagram');
    // Route::post('crawl-twitter', 'CrawlController@crawlTwitter')->name('crawl.twitter');

    // // Get list user
    // Route::get('/get-user', 'IframeController@getListUser')->name('iframe.get-user');

    // Route::get('/get-detail-iframe/{iframe_id}', 'IframeController@getDetailIframe');
    // Route::get('/get-detail-draft/{iframe_id}', 'IframeDraftController@getIframeDraft');

    // // Import csv
    // Route::post('/import-instagram-link', 'ImportCsvController@importInstagram');
    // Route::post('/import-twitter-link', 'ImportCsvController@importTwitter');

    // // Accounts Resource Controller
    // Route::resource('accounts', 'AccountController');

    // // Upload review avatar
    // Route::post('/upload-image-review', 'IframeController@uploadImgReview')->name('image.upload');

    // Route::post('/accounts/create', 'AccountController@sendMail')->name('accounts.send-mail');

