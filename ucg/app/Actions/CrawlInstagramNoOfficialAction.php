<?php

namespace App\Actions;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use InstagramScraper\Instagram;

final class CrawlInstagramNoOfficialAction
{
    /**
     * @param  string $link;
     * @return
     */
    public function handle(string $link)
    {
        try {
            $start = strpos($link, "/p/");

            if ($start) {
                $instagram_id = substr($link, $start + 3);
                $end_offset = strpos($instagram_id, '/');
                
                if ($end_offset !== false) {
                    $instagram_id = substr($instagram_id, 0, $end_offset);
                }
                
                $instagram = Instagram::withCredentials(new Client(), 'ninhndt_rabiloo', 'a12345678X', Cache::store());
                $instagram->login();
                $media = $instagram->getMediaByCode($instagram_id);

                // List image of post
                $images = [];

                if (count($media->getCarouselMedia())) {
                    foreach ($media->getCarouselMedia() as $item_media) {
                        $img_link = $this->saveImageInstagram($item_media);
                        array_push($images, $img_link);
                    }
                } else {
                    $img_link = $this->saveImageInstagram($media);
                    array_push($images, $img_link);
                }

                $data = [
                    'link' => $link,
                    'account' => $media->getOwner()->getUsername(),
                    'content' => $media->getCaption(),
                    'date' => date('Y-m-d', (int) $media->getCreatedTime()),
                    'image' => json_encode($images)
                ];

                return $data;
            }

            return [];
        } catch (Exception $ex){
            return [];
        }
    }

    public function saveImageInstagram($media)
    {
        if ($media->getType() === 'image') {
            $thumbnail_url = $media->getImageHighResolutionUrl();
        } else {
            $thumbnail_url = $media->getVideoStandardResolutionUrl();
        }

        // Get content file
        $contents = file_get_contents($thumbnail_url);

        // Get file name
        $media_name = substr($thumbnail_url, strrpos($thumbnail_url, '/') + 1);
        $name = substr($media_name, 0, strrpos($media_name, '?'));

        $url = 'instagrams/' . $name;
        Storage::put($url, $contents);

        return [
            'type' => $media->getType(),
            'link' =>  $url
        ];
    }
}
