<?php

namespace App\Actions;

use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

final class CrawlInstagramLinkAction
{
    /**
     * @param  string $link;
     * @return
     */
    public function handle(string $link)
    {
        try {
            $link_crawl = 'https://graph.facebook.com/v11.0/instagram_oembed?url=' . $link .
            '&access_token=' . env('EMBED_INSTAGRAM');
            $client = new Client();
            $response = $client->get($link_crawl);

            if ($response->getStatusCode() == 200) {
                $body = $response->getBody();
                $content = $body->getContents();
                $result = json_decode($content);
                $thumbnail = $result->thumbnail_url;
            }

            // Get content file
            $contents = file_get_contents($thumbnail);

            // Get file name
            $media_name = substr($thumbnail, strrpos($thumbnail, '/') + 1);
            $name = substr($media_name, 0, strrpos($media_name, '?'));

            $url = 'instagrams/' . $name;
            Storage::put($url, $contents, 'public');

            $data = [
                'link' => $link,
                'account' => $result->author_name,
                'content' => '',
                'date' => Carbon::now(),
                'image' => json_encode([$url])
            ];

            return $data;
        } catch (Exception $ex){
            return null;            
        }
    }
}
