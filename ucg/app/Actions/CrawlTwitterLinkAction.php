<?php

namespace App\Actions;

use Exception;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Thujohn\Twitter\Facades\Twitter;
use App\Actions\CrawlTwitterLinkAction;
use Illuminate\Support\Facades\Storage;

final class CrawlTwitterLinkAction
{
    /**
     * @param  string $link;
     * @return
     */
    public function handle(string $link)
    {
        // Get url
        $offset = strpos($link, "/status/");
        if ($offset) {
            $twitter_id = substr($link, $offset + 8);
            if (strpos($twitter_id, "?") !== false) {
                $twitter_id = substr($twitter_id, 0, strpos($twitter_id, "?"));
            }

            try {
                $response = Twitter::getTweet($twitter_id);
                $images = [];

                if (isset($response->extended_entities) && $response->extended_entities) {
                    foreach ($response->extended_entities->media as $media) {
                        array_push($images, $media->media_url_https);
                    }
                }
                $data = [
                    'link' => $link,
                    'logo' => $response->user->profile_image_url_https,
                    'account' => $response->user->name,
                    'content' => $response->text,
                    'date' => date('Y-m-d', strtotime($response->created_at)),
                    'image' => count($images) ? json_encode($images) : null
                ];

                return $data;
            } catch (Exception $e) {
                return null;
            }
        }
    }
}
