<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class InstagramCrawledResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $list_media = [];
        $medias = json_decode($this->image, true);

        if (count($medias)) {
            foreach ($medias as $item) {
                array_push($list_media, [
                    'type' => $item['type'],
                    'link' => Storage::url($item['link'])
                ]);
            }
        }

        return [
            'id' => $this->id,
            'order' => $this->order,
            'link' => $this->link,
            'account' => $this->account,
            'content' => $this->content,
            'description' => $this->content,
            'date' => $this->date,
            'image' => $list_media,
            'status' => true
        ];
    }
}
