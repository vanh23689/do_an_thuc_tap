<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IframeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'account_name' => $this->user->username,
            'name' => $this->name,
            'user_id' => $this->user_id,
            'code' => $this->code
        ];
    }
}
