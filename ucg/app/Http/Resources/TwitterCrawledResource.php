<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TwitterCrawledResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $list_image = [];
        $images = json_decode($this->image, true);

        if ($this->image) {
            foreach ($images as $item) {
                array_push($list_image, $item);
            }
        }

        return [
            'id' => $this->id,
            'order' => $this->order,
            'link' => $this->link,
            'logo' => $this->logo,
            'account' => $this->account,
            'content' => $this->content,
            'description' => $this->content,
            'date' => $this->date,
            'image' => $list_image,
            'status' => true
        ];
    }
}
