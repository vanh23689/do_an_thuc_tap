<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IframeDraftResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'account_name' => $this->user ? $this->user->username : '',
            'name' => $this->name
        ];
    }
}
