<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // $data = $this->all();

        return [
            '*.email' => 'required|email|unique:users,email,NULL,id',
            '*.password' => 'required|min:6',
            '*.confirm_password' => 'required|same:*.password',
            '*.username' => 'required',
            '*.department' => 'required',
            '*.responsible' => 'required',
            '*.url_company' => 'required',
            '*.url_frame' => ' required',
        ];
    }

    public function messages()
    {
        return [
            '*.email.required' => 'Vui lòng nhập email',
            '*.password.required' => 'Vui lòng nhập mật khẩu',
            '*.username.required' => 'Vui lòng nhập tên tài khoản',
            '*.department.required' => 'Vui lòng nhập tên công ty',
            '*.responsible.required' => 'Vui lòng nhập tên người phụ trách',
            '*.url_company.required' => 'Vui lòng nhập đường dẫn công ty',
            '*.url_frame.required' => 'Vui lòng nhập đường dẫn frame'
        ];
    }
}
