<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Redirect;

class RegisterController extends Controller
{
    public function showRegistrationForm()
    {
        if (session()->has('exist_user')) {
            return inertia('register', [
                'exist_user' => session('exist_user')
            ]);
        }
        return Inertia::render('Auth/Register');
    }

    protected function create(RegisterRequest $data)
    {

        $user = User::create([
            'email' => $data->formData['email'],
            'password' => Hash::make($data->formData['password']),
            'username' => $data->formData['username'],
            'company' => $data->formData['company'],
            'department' => $data->formData['department'],
            'responsible' => $data->formData['responsible'],
            'url_company' => $data->formData['url_company'],
            'url_frame' => $data->formData['url_frame'],
            'types' => 2
        ]);
        if($user){
            return redirect()->route('admin.login')->with([
                'isSuccess' => true
            ]);
        }else{
            return back();
        }
    }
}
