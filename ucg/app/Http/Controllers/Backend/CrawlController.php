<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\TwitterCrawled;
use App\Models\InstagramCrawled;
use App\Http\Controllers\Controller;
use App\Actions\CrawlTwitterLinkAction;
use App\Actions\CrawlInstagramLinkAction;
use App\Http\Resources\TwitterCrawledResource;
use App\Actions\CrawlInstagramNoOfficialAction;
use App\Http\Resources\InstagramCrawledResource;

class CrawlController extends Controller
{
    public function crawlInstagram(Request $request)
    {
        //code...
        $links = $request->input('links');
        $array_link = [];

        foreach ($links as $item) {
            array_push($array_link, $item['link']);
            $crawled = InstagramCrawled::where('link', $item['link'])->first();

            if (!$crawled) {
                // Crawl new link
                $action = new CrawlInstagramNoOfficialAction();
                $data = array_merge($action->handle($item['link']), ['order' => $item['order']]);
                
                if(!empty($data)) {
                    InstagramCrawled::create($data);
                } else {
                    $action = new CrawlInstagramLinkAction();
                    $data = array_merge($action->handle($item['link']), ['order' => $item['order']]);
                    
                    if (!empty($data)) {
                        InstagramCrawled::create($data);
                    }
                }
            } else {
                // Update link
                $crawled->order = $item['order'];
                $crawled->save();
            }
        }

        $crawls = InstagramCrawled::whereIn('link', $array_link)->orderBy('order', 'asc')->get();
        return response()->json([
            'crawls' => InstagramCrawledResource::collection($crawls)
        ]);   
        
    }

    public function crawlTwitter(Request $request)
    {
        $links = $request->input('links');
        $array_link = [];

        foreach ($links as $item) {
            array_push($array_link, $item['link']);
            $crawled = TwitterCrawled::where('link', $item['link'])->first();
            
            if (!$crawled) {
                // Crawl new link
                $action = new CrawlTwitterLinkAction();
                $data = array_merge($action->handle($item['link']), ['order' => $item['order']]);

                if(!empty($data)){
                    TwitterCrawled::create($data);
                }
            } else {
                // Update link
                $crawled->order = $item['order'];
                $crawled->save();
            }
        }

        $crawls = TwitterCrawled::whereIn('link', $array_link)->orderBy('order', 'asc')->get();

        return response()->json([
            'crawls' => TwitterCrawledResource::collection($crawls)
        ]);
    }
}
