<?php

namespace App\Http\Controllers\Backend;

use App\Models\Iframe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\IframeResource;

class IframeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->types == 1) {
            $iframes = Iframe::orderBy('id', 'desc')->get();
        } else {
            $iframes = Iframe::where('user_id', auth()->user()->id)
                ->orderBy('id', 'desc')
                ->get();
        }

        return inertia('backend/iframes/list', [
            'iframes' => IframeResource::collection($iframes),
            // 'current_page' => 1,
            // 'countRows' => count($iframes)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return inertia('Iframe/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
