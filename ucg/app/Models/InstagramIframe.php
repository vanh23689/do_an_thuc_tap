<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramIframe extends Model
{
    protected $table = 'instagram_iframes';

    protected $fillable = [
        'iframe_id', 'link', 'status'
    ];

    public function iframes(){
        return $this->hasOne(Iframe::class);
    }
}
