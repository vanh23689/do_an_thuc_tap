<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use InstagramScraper\Instagram;

class Iframe extends Model
{
    protected $table = 'iframes';

    protected $fillable = ['name', 'type', 'user_id', 'code'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function instagrams()
    {
        return $this->hasMany(InstagramIframe::class, 'iframe_id');
    }

    public function twitters()
    {
        return $this->hasMany(TwitterIframe::class, 'iframe_id');
    }

    public function reviews()
    {
        return $this->hasMany(ReviewIframe::class, 'iframe_id');
    }
}
