<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramCrawled extends Model
{
    protected $table = 'instagram_crawleds';

    protected $fillable = [
        'order', 'link', 'account', 'content', 'date', 'image'
    ];
}
