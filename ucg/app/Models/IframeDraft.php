<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IframeDraft extends Model
{
    use SoftDeletes;
    protected $table = 'iframe_drafts';

    protected $fillable = [
        'name', 'type', 'user_id', 'instaLinks', 'twitLinks', 'reviews'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
