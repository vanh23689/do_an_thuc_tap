<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwitterIframe extends Model
{
    protected $table = 'twitter_iframes';

    protected $fillable = [
        'iframe_id', 'link', 'status'
    ];

    public function iframes(){
        return $this->hasOne(Iframe::class);
    }
}
