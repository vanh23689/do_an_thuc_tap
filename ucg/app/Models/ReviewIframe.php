<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewIframe extends Model
{
    protected $table = 'review_iframes';

    protected $fillable = [
        'iframe_id', 'username', 'date_publish', 'content',
        'image_1', 'image_2', 'image_3'
    ];

    public function iframes(){
        return $this->hasOne(Iframe::class);
    }
}
