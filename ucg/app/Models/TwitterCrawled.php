<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwitterCrawled extends Model
{
    protected $table = 'twitter_crawleds';

    protected $fillable = [
        'order', 'link', 'logo', 'account', 'content', 'date', 'image'
    ];
}
